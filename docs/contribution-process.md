# Contribution process

This guide covers the expectations and processes for Apertis developers wishing
to make contributions to the Apertis project and the wider open source
ecosystem. These policies should be followed by all developers, including core
and third party contributors.


## Suitability of contributions

Like most open source projects, Apertis requires contributions are submitted
via a process (which in the case of Apertis is defined below) to ensure that
Apertis continues to meet it's design goals and remain suitable for it's
community of users. In addition to design and technical implementation details,
the suitability of contributions will be checked to meet requirements in areas
such as [coding conventions](coding_conventions.md) and
[licensing](license-expectations.md).


### Upstream First Policy

Apertis is a fully open source GNU/Linux distribution that carries a lot of
components for which it is not the upstream. The goal of
[upstream first](upstreaming.md) is to minimize the amount of deviation and
fragmentation between Apertis components and their upstreams.

Deviation tends to duplicate work and adds a burden on the Apertis developers
when it comes to testing and updating to newer versions of upstream components.
Also, as the success of Apertis relies on the success of open source in general
to accommodate new use cases, it is actively harmful for Apertis to not do its
part in moving the state of the art forward.

It is the intention of Apertis to utilize existing open source projects to
provide the functionality required, where suitable solutions are available,
over the creation of home grown solutions that would fragment the GNU/Linux
ecosystem further.

This policy should be taken into consideration when submitting contributions to
Apertis.


### Upstream Early, Upstream Often

One mantra that can be often heard in Open Source communites is "upstream early,
upstream often". The approach that this espouses is to breakdown large changes
into smaller chunks, attempting to upstream a minimal implementation before
implementing the full breath of planned features.

Each open source community tends to be comprised of many developers, which
share some overlap between their goals, but may have very different focuses. It
is likely that other developers contributing to the project may have ideas
about how the features that you are planning may be better implemented, for
example to enable a broader set of use cases to utilise the feature. Submitting
an early minimal implementation allows the general approach to be assessed,
opinions to be sought and a concensus reached regarding the implementation. As
it is likely that some changes will be required, a minimal implementation
minimizes the effort required to take feedback into account.

Taking this approach a step further, it can often be instructive to share your
intention to implement larger features before starting. Such a conversation
might be started by sending an email to the projects devel
[mailing list](https://lists.apertis.org/) saying:

```
Hi,

I'm attempting to use <project> to <task> for my project.

I'm thinking about doing <brief technical overview> to enable this usecase.

I'm open to suggestions should there be a better way to solve this.

Thanks,

<developer>
```

This enables other experienced developers the chance to suggest approaches that
may prove to be the most efficient, saving effort in implementation and later
in review, or may point to missed existing functionality that can be used to
solve a given need without needing substancial development effort.


## Extending Apertis

### Adding components to Apertis

Apertis welcomes requests for new components to be added to the distribution
and can act as a host for projects where required, however the open source
focus of Apertis should be kept in mind and any proposed contributions need to
both comply with Apertis policies and present a compelling argument for
inclusion.

Additional components can be categorised into 3 main groups:

- Existing upstream component available in Debian stable (with suitable version)
- Existing upstream component, not available in debian stable
- New component on gitlab.apertis.org

There is a maintenance effort associated with any components added to Apertis,
as any components added will need to be maintained within the Apertis
ecosystem. The effort required to maintain these different categories of
components are very different. Prepackaged Debian components require a lot less
maintenance effort than packaging other existing upstream components.
Developing a new component on gitlab.apertis.org requires both the development
and packaging/maintenance to be carried out within Apertis, significantly
raising the effort required.

When looking for ways to fullfil a requirement there are a number of factors
that will increase the probability of a solution being acceptable to Apertis.

- Component already included in Debian stable: As Apertis is based on Debian
  and already has processes in place to pull updates from this source. The cost
  of inclusion is dramatically lower than maintaining packages drawn from other
  sources, as a lot of the required effort to maintain the package is being
  carried out within the Debian ecosystem.
- Proven actively maintained codebase: Poorly maintained codebases present a
  risk to Apertis, increasing the chance that serious bugs or security holes
  will go unnoticed. Picking a solution that has an active user base, a
  developer community making frequent updates and/or is a mature codebase that
  has undergone significant "in the field" testing makes the solution more
  attractive for inclusion in Apertis. It is understood that, whilst extensive,
  the Debian repositories are not all encompassing, if proposing an existing
  open source component that isn't currently provided by Debian, being able to
  show that it is actively maintained will be important.
- Best solution: In general, there exists more open source solutions than there
  exists problems. To be in with a good chance of having a component included
  in Apertis it will be required to explain why the chosen solution represents
  the best option for Apertis. What is "best" is often nuanced and will be
  affected by a number of factors, including integration/overlap with existing
  components and the size/number of dependencies it has (especially if they
  aren't currently in Apertis). It may be that whilst a number of existing
  solutions exist, none of them are a good fit for Apertis. This may suggest a
  new component is the best solution, though adapting/extending one of the
  existing solutions should also be considered.

The Apertis distribution is supported by it's members. As previously mentioned,
in order to ensure that Apertis remains viable and correctly focused, it is
important that any additions to the main
[Apertis projects](https://wiki.apertis.org/Package_maintenance) are justified
and can be shown to fill a specific and real use case. Maintaining the
packaging, updating the codebases of which Apertis is comprised and performing
testing on supported platforms is a large part of the effort needed to provide
Apertis. As a result, it will be necessary to either be able to provide a
commitment to support any packages proposed for inclusion in the main Apertis
projects or gain such a commitment from an existing member.

The Apertis development team commit to maintaining the packages included in the
references images. Packages may be added to the main package repositories but
not form part of the reference images. Such packages will be maintained on a
best effort basis, that is as long as the effort remains reasonable the Apertis
team will attempt to keep the package in a buildable state, however runtime
testing will not be performed. Should the package fail to build or runtime
issues are reported and significant effort be required to modify the package
the original or subsequent users of the package may be approached to help
resource fixing the package. Ultimately the package may be removed if a
solution can not be found. Likewise, should a different common solution for
Apertis be chosen at a later date, the package may be deprecated and
subsequently removed.

Proposals for inclusion of new components are expected to be made in the form
of a written proposal. Such a proposal should contain the following
information:

- Description of the problem which is being addressed
- Why the functionality provided by the proposed component is useful to Apertis
  and it's audience
- A review of the possible solutions and any advantages and disadvantages that
  have been identified with them
- Why the proposed solution is thought to present the best way forward, noting
  the points made above where relevant
- Whether any resources are to be made available to help maintain the component.

An alternative to adding packages to the main Apertis project is to apply to
have a dedicated project area, where code specific to a given project can be
stored. Such an area can be useful for providing components that are highly
specific to a given project and/or as a staging area for modifications to core
packages that might later get folded back into the main area, either by changes
being submitted to the relevant Apertis component or after changes have been
[upstreamed](upstreaming.md) to the components main project. A dedicated area
will allow a project group to iterate on key components more rapidly as the
changes made do not need to work across the various supported hardware
platforms. It must be noted that whilst a dedicated project area would allow
some requirements with regard to platform support to be ignored, packages in
such areas would still be required to comply with other Apertis rules such as
[open source licensing](license-expectations.md). It should be expected that
the Apertis developers will take a very hands off approach to the maintenance
and testing of packages in such areas. If packages in such areas require work,
the project maintainers will be contacted. The Apertis maintainers may at their
discresion help with minor maintenance tasks should a package be of interest to
the Apertis project. Packages that become unmaintained may be removed.

Requests for dedicated project areas are also expected to be made in a form of
a written proposal. Such a proposal should contain the following information:

- Description of the project requiring a dedicated project area
- Preferred name to be used to refer to the project
- Expected use of the dedicated area
- Expected lifetime of the project area
- Contact details of project maintainers

Such submissions should be made via the devel [mailing list](https://lists.apertis.org/).

The submission should be discussed on the mailing list and must be agreed with
the Apertis stakeholders.


### Extending existing components

Apertis carries a number of packages that have been modified compared to their
upstream versions. It is fairly typical for distributions to need to make minor
modifications to upstream sources to tailor them to the distribution, Apertis
is not different in this regard.

Whilst Apertis does accept changes to existing components, it needs to be
acknowledged that this increases the effort required to maintain the package in
question. It may be requested that an attempt be made to upstream
the changes, in line with the [upstream first](#upstream-first-policy) policy,
either to the packages upstream or Debian. More guidance is provided in the
[upstreaming](upstreaming.md) documentation.


## Development Process

The process for contributing code to Apertis is the same as for many other open
source projects: check out the code, make changes locally, then submit them for
review and merging. Apertis uses its GitLab instance as a code repository and
review tool.

This guide assumes good working knowledge of git (see
[Version control](https://wiki.apertis.org/Guidelines/Version_control) for
specific guidelines) and an account with commit rights on GitLab (see
[Getting commit rights](#getting-commit-rights) for guidance).


### Branch

Propose changes by pushing a git branch to GitLab.

* Check out the git repository:
```
git clone https://gitlab.apertis.org/[group name]/[repository name].git
```
* Create a local branch and make code changes in there. The branch should have
  the following naming convention: `wip/$username/$feature` or
  `wip/$username/$taskid`. This branch should be pushed into the destination
  repository: if developers lack permissions to do so they can ask to be
  granted the needed privileges on the developer
  [mailing list](https://lists.apertis.org/) or push to a personal repository
  instead.
* Commit the changes (see the
  [guidelines for making commits](https://wiki.apertis.org/Guidelines/Version_control#Guidelines_for_making_commits)
  ), test them and check you are allowed to submit them under the project’s
  license, following the [sign-offs](#signoffs) documentation
```
git commit -i -s
```
* Every commit must have an appropriate [`Signed-off-by:` tag](#signoffs) in
  the commit message.
* Add a `Fixes: APERTIS-<task_id>` tag for each task in the proposed commit
  messages (as explained in the section "Automatically closing tasks" below or
  in the envelope message of the merge request itself in order to link the
  merge request to one or more tasks in Phabricator.
* Note: The tag will ensure that Phabricator tasks are kept up-to-date with
  regard to the status of related merge requests, through the creation of a new
  comment with the link to the merge request every time a merge request is
  created/updated/merged. This syntax has been chosen for the tag because it is
  already
  [supported by gitlab](https://docs.gitlab.com/ce/user/project/integrations/custom_issue_tracker.html).


### Merge Request

* Once the changes are ready to be reviewed, create a
  [merge request on GitLab](https://docs.gitlab.com/ce/user/project/merge_requests/).
* The merge request should have the "Remove source branch when merge request
  accepted" option enabled.
* If changes are not ready and some (strongly encouraged) early feedback is
  required, the merge request should be
  [marked as Work In Progress (WIP)](https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html).


### Review

* Notify maintainers on the devel [mailing list](https://lists.apertis.org/)
  when submitting or updating a merge request.
* The repository maintainer or another developer will review the merge request.
* Reviews should happen on GitLab.
* Reviewers can propose concrete code snippets that the submitter can decide to
  integrate in their patch series or not:
  * Reviewers can use code blocks in comments, and the submitter can copy and
    paste them in their patches when updating the merge request
  * Reviewers can edit the submitted branch with the GitLab Web IDE or checking
    it out manually, stacking new commits on top of the submitted patches: it's
    up to the submitter to cherry-pick or reject the suggested changes and
    squash them appropriately with the original commits before the request can
    be merged
  * If submitters do not agree with the suggested snippets they need to explain
    why they are rejecting the proposed changes
* If changes are needed, a force-push with the changes is required to the
  submitted branch to update the commits that are part of the merge request
  (always remember to
  [use `--force-with-lease`](https://developer.atlassian.com/blog/2015/04/force-with-lease/)
  instead of `--force`).
* Questions, request to clarify and any other kind of discussion should be
  handled via comments on the merge request.
* Repeat these review steps until all the changes are accepted.


### Merge

* Merge request must have no thumbs down to be merged and all the
  [discussions should be resolved](https://docs.gitlab.com/ce/user/project/merge_requests/#resolve-discussion-comments-in-merge-requests-reviews).
* Reviewers that approve the general approach but still want to discuss some
  details can add a thumb up, so other reviewers know that once all the
  discussions are resolved the merge request can be approved
* Once all comments have been handled, the reviewer hits the merge button or
  otherwise pushes the submitted commits to the appropriate branch.


### Extra Actions

* If other actions need to be manually taken when commits are landed (for
  instance, updating the jobs on Jenkins when changing the templates) the
  developer accepting and merging the changes is responsible for ensuring that
  those actions are taken, either by doing them themselves or by asking someone
  else to do that.
* If the merged commits need to be backported to other branches through
  cherry-pick or merges, those should go through merge requests as well:
  however, only changes related to the backport process are allowed, unrelated
  changes should be applied with a separate merge request, possibly landing in
  master first.


### Release Commits

* Changes to the `debian/changelog` file of the package should leave the new
  version marked as `UNRELEASED` during the review.
* Once all the changes have been accepted and merged, a release commit should
  be made. This release commit can be pushed directly without going through
  merge requests.
* The release commit should *only* update the `debian/changelog` file changing
  the `UNRELEASED` tag to `apertis`.
* A tag should be generated for this commit and pushed to the repository
  **before** pushing the release commit itself.
* Once the tag has been pushed, the release commit should be pushed directly to
  the repository, without any review: the infrastructure will automatically
  trigger a new package build with the new version as expected.
* **Note**: The developer doing the release commit might need special
  permissions in the repository to push tags and commit the changes directly
  without any review.


# Other important bits

## Sign-offs

Like the git project and the Linux kernel, Apertis requires all contributions
to be signed off by someone who takes responsibility for the open source
licensing of the code being contributed. The aim of this is to create an
auditable chain of trust for the licensing of all code in the project.

Each commit which is pushed to git master **must** have a `Signed-off-by` line,
created by passing the `--signoff`/`-s` option to `git commit`. The line must
give the real name of the person taking responsibility for that commit, and
indicates that they have agreed to the
[Developer Certificate of Origin](http://developercertificate.org/). There may
be multiple `Signed-off-by` lines for a commit, for example, by the developer
who wrote the commit and by the maintainer who reviewed and pushed it:

```
Signed-off-by: Random J Developer <random@developer.example.org>
Signed-off-by: Lucky K Maintainer <lucky@maintainer.example.org>
```

Apertis closely follows the Linux kernel process for sign-offs, which is
described in section 11 of the
[kernel guide to submitting patches](https://www.kernel.org/doc/Documentation/SubmittingPatches).


## Automatically closing tasks

You should never close a task manually. To automatically close the task when
your changes appear in the packages, add a meta-tag in the git commit message
description, pointing to one or more Apertis tasks, each on its own line:

```
Fixes: APERTIS-<task_id>
```

When `debian/changelog` file is generated from the commit messages by
`gbp dch`, that meta-tag is transformed into `(Fixes: APERTIS-T12345)` appended
to the changelog message. When the package is built and uploaded, the task will
be automatically closed.

By default, only the first line of the commit message is used for the changelog
entry.

For example, for a commit message like this:

```
Update website link to demo images

The website has new path for demo images.

Fixes: APERTIS-T12345
Thanks: John Smith for pointing out the issue

Signed-off-by: Random J Developer <random@developer.example.org>
```

will be converted into the following changelog entry:

```
* Update website link to demo images.
  Thanks to John Smith for pointing out the issue (Fixes: APERTIS-T12345)
```


## Privileged processes

Pushing commits to gitlab.apertis.org requires commit rights which are only
granted to trusted contributors (see
"[Getting commit rights](#getting-commit-rights)" for how to get
-commit rights). All commits must have a
[`Signed-off-by` line](#signoffs) assigning responsibility for their open
source licensing.

Packaging up and releasing new versions of Apertis modules as Debian packages
requires packaging rights on build.collabora.co.uk (OBS), which are issued
separately from commit rights.

Submitting automated test runs on lava.collabora.co.uk requires CI rights,
which are granted similarly to packaging rights. However, CI results may be
viewed read-only by anyone.


### Getting commit rights

Commit rights (to allow direct pushes to git, and potentially access to the
package building system, build.collabora.co.uk) may be granted to trusted third
party contributors if they regularly contribute to Apertis, with high quality
contributions at the discretion of current Apertis maintainers.

If you wish to acquire an account on the Apertis GitLab instance or other
Apertis infrastructure, please send an email to
`account-requests@apertis.org` including:

- Your full name
- The email address you prefer to be contacted through
- The nickname/account name you wish to be known by on the Apertis GitLab


## API review process

Before a public API may be marked as stable and suitable for use by third-party
apps, it must be reviewed. The process for this is:
* Some or all of the APIs of a component are selected for review. For example,
  it might be decided to review the C APIs for a component, but not the D-Bus
  APIs which it uses internally to communicate with a background service.
* The component authors produce:
  * A description of the goals of the component and the APIs under review.
    These should be high-level descriptions, covering the use cases for that
    component and its APIs, goals of the implementation, and specific
    non-goals. Where possible, this should refer back to a design document.
    These descriptions should *not* be simply a copy of the API documentation
    describing *how* the goals are implemented.
  * A list of required features and APIs which must pass review before the API
    may be marked as stable.
  * A list of other components which depend on the APIs under review, and a
    description of how those other components should interact with the APIs
    under review. This ensures the final APIs are still usable as intended from
    other components, and is another way of describing the use cases of the
    API.
  * An explicit description of where privilege boundaries are required in the
    system, and whether any of the APIs cross those boundaries (for example, if
    they implement functionality which should not be callable without elevated
    privileges).
  * A description of how the API may need to be extended. For example, should
    it be extensible by the component maintainers, by third-party apps, or not
    at all?
* The reviewers examine the APIs and provided documentation, and make
  recommendations for the component as a whole, its interactions with other
  components, and specific recommendations for each API under review (for
  example, ‘keep this API unchanged’, ‘remove this API’, ‘keep it with the
  following changes’, etc.). See the
  [API design guidelines](https://wiki.apertis.org/Guidelines/API_design) for
  some principles that the reviewers might apply.
* The feedback is discussed, potentially iterated, and changes are made to the
  public API accordingly.
* The changes are re-reviewed, and the process iterates until the reviewers are
  happy.
* The APIs are marked as public and stable, and their API documentation is
  updated.

The main goal of the review process is to ensure that the stable APIs, which
have to be supported for a long time in the future, are a good match for all
current and anticipated use cases, and follow good system architecture
practices. A large part of this effort is in ensuring that all use cases for
the APIs are known and documented. A *non-goal* of the review process is to
look at each API individually and mechanically run through some ‘API review
checklist’ — this can easily miss mismatches between the API and the
component’s use cases; architectural issues which are important to catch.

Components’ APIs should not be marked as stable until they have been reviewed.

