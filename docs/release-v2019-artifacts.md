---
title: Release artifacts for Apertis v2019 [draft]
short-description: Draft of the artifacts planned for the Apertis v2019 release
authors:
   - name: Emanuele Aina
...

# Introduction

This draft document describes which artifacts are expected to be part of the
Apertis v2019 release and what are their goals.

The main kinds of artifacts are:

* ospack: rootfs without the basic hardware-specific components like bootloader, kernel and hardware-specific support libraries
* system image: combines an ospack with hardware-specific components in a snapshot that can be directly deployed on the supported boards
  * Apt-based images: images meant for development, with a modifiable rootfs that can be customized with the [Apt](https://wiki.debian.org/Apt) package manager
  * OSTree-based images: images with a immutable rootfs and a reliable update mechanism based on [OSTree](https://ostree.readthedocs.io/), more similar to what products would use than the Apt-based images
* OSTree repository: server backend used by the OSTree-based images for efficient distribution of updates
* sysroot: rootfs to be used for cross-compilation for platform and application development targeting a specific image
* devroot: rootfs for targeting foreign platforms using binary emulation for platform and application development
* nfs: kernel, initrd, dtb and rootfs tarball for network booting using TFTP and NFS

# Supported platforms

Architectures:

  * `amd64`: the common Intel x86 64bit platform, also known as `x86-64`
  * `armhf`: the hard-float variant of the ARMv7 32bit platform
  * `arm64`: the ARMv8 64bit platform, also known as `aarch64`

[Reference systems](https://wiki.apertis.org/Reference_Hardware):
  * `amd64`: [MinnowBoard Turbot Dual-Core (E3826)](https://wiki.apertis.org/Reference_Hardware/Minnowboard_setup)
  * `amd64`: [VirtualBox](https://wiki.apertis.org/VirtualBox) for the SDK virtual machines
  * `armhf`: [i.MX6 Sabrelite](https://wiki.apertis.org/Reference_Hardware/imx6q_sabrelite_setup)
  * `arm64`: [Renesas R-Car M3 Starter Kit Pro (M3SK/m3ulcb)](https://wiki.apertis.org/Reference_Hardware/Rcar-gen3_setup)

# Supported artifacts

This is an overview of the release artifacts:

* `minimal` ospacks
  * amd64, armhf, arm64
  * OSTree repository
  * Apt-based and OSTree-based system images for the reference hardware platforms
* `target` ospacks
  * amd64, armhf, arm64
  * OSTree repository
  * Apt-based and OSTree-based system images for the reference hardware platforms
* `basesdk` ospacks
  * amd64
  * Apt-based system image for VirtualBox
* `sdk` ospacks
  * amd64
  * Apt-based system image for VirtualBox
* `sysroot` ospack
  * amd64, armhf, arm64
  * tarball matching the target ospack
* `devroot` ospack
  * amd64, armhf, arm64
  * tarball
* `nfs` ospack
  * amd64, armhf, arm64
  * tarball and unpacked kernel artifacts for TFTP/NFS network booting

## Minimal

Minimal images provide compact example images for headless systems and
are a good starting point for product-specific customizations.

Other than basic platform support in order to succesfully boot on the reference
hardware, the minimal example images ship the complete connectivity stack.

The reference update system is based on OSTree, but APT-based images are also
provided for development purposes.

No artifact covered by the GPLv3 is part of the `minimal` ospacks and images.

## Target

Target images provide the most complete superset of the features offered by the minimal images,
shipping full graphics support with a sample HMI running on top and
applications with full multimedia capabilities.

The reference update system is based on OSTree, but APT-based images are also
provided for development purposes.

No artifact covered by the GPLv3 is part of the `target` ospacks and images.

The release also includes sysroots which include development tools, headers,
and debug symbols for all the components shipped on target images. These
sysroots contains software covered by the GPLv3 as they are meant for
development purposes only.

## Base SDK

The base SDK images are meant to be run as VirtualBox VMs to provide a standardized,
ready-to-use environment for developers targeting Apertis, both for platform
and application development.

Since the SDK images are meant for development, they also ship tools covered by
the GPLv3 license.

## SDK

The full SDK images provide the same features as the base SDK images with
additional tools for application development using the Canterbury application
framework and the Mildenhall HMI.

## Sysroot

Sysroots are filesystem trees specifically meant for cross-compilation and
remote debugging targeting a specific release image.

They are meant to be read-only and target a specific release image, shipping
all the development headers and debug symbols for the libraries in the
release image.

Sysroots can be used to cross-compile for Apertis from a third-party
environment using an appropriate cross-toolchain. They are most suited for
early development phases where developers focus on quick iterations and rely
on fast incremental builds of their components.

## Devroot

Devroots are filesystem trees meant to offer a foreign architecture build
environment via containers and binary emulation via the QEMU user mode.

They ship a minimal set of packages and offer the ability to install all the
packages in the Apertis archive.

Due to the nature of foreign architecture emulation they impose a considerable
overhead on build times compared to sysroot, but they avoid all the intricacies
that cross-building involves and offer the ability to reliably build deb packages
targeting foreign architectures.

For more informations about using devroots see the
["Programming guidelines" section](https://developer.apertis.org/latest/programming-guide-tooling.html#development-containers-using-devrootenter).

## NFS

The release includes artifacts for network booting using the TFTP and NFS protocols:
* kernel images for the reference architectures to be loaded via TFTP
* initrd with kernel modules matching the image to be loaded via TFTP
* DTBs (compiled device trees) for the reference hardware platforms to be loaded via TFTP
* rootfs tarball to be loaded via NFS
