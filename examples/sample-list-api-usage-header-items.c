#include "sample-list.h"

static SampleListItem *
create_sample_artist_item (const gchar *name)
{
  SampleListItem *res = sample_list_item_new ();
  ClutterActor *name_label = clutter_text_new_with_text (name, "Sans 12");

  clutter_actor_add_child (CLUTTER_ACTOR (res), name_label);

  return res;
}

static SampleListItem *
create_sample_header_item (const gchar *header_text)
{
  SampleListItem *res = sample_list_item_new ();
  ClutterActor *name_label =
      clutter_text_new_with_text (header_text, "Sans 12");

  clutter_actor_add_child (CLUTTER_ACTOR (res), name_label);
  g_object_set (res, "selectable", FALSE, NULL);

  return res;
}

static SampleList *
create_sample_list (void)
{
  GListStore *adapter;
  SampleList *list;

  adapter = g_list_store_new (SAMPLE_TYPE_LIST_ITEM);

  g_list_store_append (adapter, create_sample_header_item ("A"));
  g_list_store_append (adapter, create_sample_artist_item ("ABBA"));
  g_list_store_append (adapter, create_sample_header_item ("B"));
  g_list_store_append (adapter, create_sample_artist_item ("Bob Marley"));

  list = sample_list_new ();
  sample_list_set_adapter (list, G_LIST_MODEL (adapter));

  g_object_unref (adapter);

  return list;
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  /* We may now expose the list to the user, once that is done ... */
  g_object_unref (list);

  return 0;
}
