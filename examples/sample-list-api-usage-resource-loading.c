#include <gdk/gdk.h>
#include <cogl/cogl.h>

#include "sample-utils.h"

static ClutterContent *
create_empty_image (void)
{
  ClutterContent *res;
  static guint8 empty[] = { 0, 0, 0, 0xff };

  res = clutter_image_new ();

  /* Here, we simply set a black background as a placeholder,
   * other implementations may use an actual image.
   */
  clutter_image_set_data (CLUTTER_IMAGE (res), empty,
                          COGL_PIXEL_FORMAT_RGBA_8888, 1, 1, 1, NULL);

  return res;
}

static void
_pixbuf_loaded_cb (GObject *source, GAsyncResult *result, gpointer user_data)
{
  g_autoptr (GdkPixbuf) pixbuf;
  GError *error = NULL;

  pixbuf = gdk_pixbuf_new_from_stream_finish (result, &error);

  if (pixbuf)
    clutter_image_set_data (
        CLUTTER_IMAGE (user_data), gdk_pixbuf_get_pixels (pixbuf),
        gdk_pixbuf_get_has_alpha (pixbuf) ? COGL_PIXEL_FORMAT_RGBA_8888
                                          : COGL_PIXEL_FORMAT_RGB_888,
        gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf),
        gdk_pixbuf_get_rowstride (pixbuf), NULL);

  g_object_unref (user_data);
}

static void
list_item_showing_cb (SampleListItem *item,
                      GParamSpec *pspec,
                      gpointer user_data)
{
  ClutterContent *image;
  GError *error = NULL;
  g_autoptr (GFile) file;
  g_autoptr (GInputStream) stream;

  /* We already loaded the image for this item */
  if (clutter_actor_get_content (CLUTTER_ACTOR (item)) != NULL)
    return;

  file = g_file_new_for_path ("file:///some/path");
  stream = G_INPUT_STREAM (g_file_read (file, NULL, &error));

  if (!stream)
    return;

  /* We only load the image once it needs to be shown to the user */
  image = create_empty_image ();

  /* We take a reference to the image, to make sure it does not get
   * disposed before loading is done
   */
  gdk_pixbuf_new_from_stream_async (stream, NULL,
                                    (GAsyncReadyCallback) _pixbuf_loaded_cb,
                                    g_object_ref (image));

  /* For simplicity, we set the image as the content of the item,
   * more advanced usage would have us set it as the content of
   * a child actor */
  clutter_actor_set_content (CLUTTER_ACTOR (item), image);
}

static void
list_item_created_cb (SampleList *list,
                      SampleListItem *item,
                      gpointer user_data)
{
  g_signal_connect (item, "notify::showing", (GCallback) list_item_showing_cb,
                    NULL);
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  /* Connect to the notify::showing signal from every list item. */
  g_signal_connect (list, "item-created", (GCallback) list_item_created_cb,
                    NULL);

  /* now display to the list to the user, then ... */
  g_object_unref (list);

  return 0;
}
