#include <glib.h>
#include <gio/gio.h>

#include "sample-list.h"

/* This is left undefined to keep the example short, modalities
 * from retrieving backing data will vary from one implementation
 * to another
 */
static guint get_chart_position_from_item (SampleListItem *item);

/* Example alternative adapter implementation
 *
 * SampleLazyList is an adapter for the list store, which adapts an underlying
 * data source to a GListModel of SampleListItem objects. In this example the
 * source of the underlying data (self->model) is left unspecified, but it
 * could be any presentation-agnostic data source, such as a database or
 * a GListModel containing non-widget objects. */

#define SAMPLE_TYPE_LAZY_LIST (sample_lazy_list_get_type ())
G_DECLARE_FINAL_TYPE (
    SampleLazyList, sample_lazy_list, SAMPLE, LAZY_LIST, GObject)

struct _SampleLazyList
{
  GObject parent_instance;

  /* cache, using a GSequence is adapted for really large models,
   * but implementations may use another data type such as GPtrArray
   * or GArray for smaller models to decrease memory overhead */
  GSequence *artists;

  /* The underlying data model. In theory it could have any type,
   * but in practice the adapter would declare a specific type that it
   * must have (for example, a connection object from an SQLite database),
   * and use that. */
  gpointer model;
};

static void sample_lazy_list_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (SampleLazyList,
                         sample_lazy_list,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                sample_lazy_list_iface_init));

static void
sample_lazy_list_finalize (GObject *obj)
{
  g_sequence_free (SAMPLE_LAZY_LIST (obj)->artists);

  G_OBJECT_CLASS (sample_lazy_list_parent_class)->finalize (obj);
}

static void
sample_lazy_list_class_init (SampleLazyListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sample_lazy_list_finalize;
}

static GType
sample_lazy_list_get_item_type (GListModel *list)
{
  return SAMPLE_TYPE_LIST_ITEM;
}

static guint
sample_lazy_list_get_n_items (GListModel *list)
{
  return 42; /* Arbitrary number, implementation-dependent */
}

static gint
sample_artist_item_find (gconstpointer p1,
                         gconstpointer p2,
                         gpointer user_data)
{
  guint pos1 = get_chart_position_from_item ((SampleListItem *) p1);
  guint pos2 = *(guint *) p2;

  return ((pos1 < pos2) ? -1 : ((pos1 == pos2) ? 0 : 1));
}

static gint
sample_artist_item_compare (gconstpointer p1,
                            gconstpointer p2,
                            gpointer user_data)
{
  guint pos1 = get_chart_position_from_item ((SampleListItem *) p1);
  guint pos2 = get_chart_position_from_item ((SampleListItem *) p2);

  return ((pos1 < pos2) ? -1 : ((pos1 == pos2) ? 0 : 1));
}

static void
sample_artist_item_unreffed (SampleListItem *item, SampleLazyList *self)
{
  GSequenceIter *iter;

  iter = g_sequence_lookup (self->artists, item, sample_artist_item_compare,
                            NULL);
  g_assert (iter);
  g_sequence_remove (iter);
}

/* Here, lookup the artist item from the self->artists sequence
 * and return it if it was found.
 *
 * Otherwise, construct a new artist item, add it to self->artists and
 * return it.
 *
 * If memory is constrained, let the created object inherit from
 * GInitiallyUnowned, take a weak reference on it and return that instead.
 *
 * When the weak reference gets notified, remove it from self->artists
 * in order to return a new object if required.
 */
static gpointer
get_from_cache (SampleLazyList *self, guint position)
{
  GSequenceIter *iter;
  SampleListItem *res;

  iter = g_sequence_lookup (self->artists, &position, sample_artist_item_find,
                            NULL);

  if (iter)
    {
      return g_sequence_get (iter);
    }

  res = sample_list_item_new ();

  /* Implementations should populate the view here, using data from some
   * underlying self->model. */

  /* ListItem will inherit from GInitiallyUnowned. Implementations may either
   * call g_object_ref_sink on the new list item or, as we do here, let List
   * assume ownership of the list item, and set a weak reference to be notified
   * when it is disposed.
   */
  g_sequence_insert_sorted (self->artists, res, sample_artist_item_compare,
                            NULL);
  g_object_weak_ref (G_OBJECT (res), (GWeakNotify) sample_artist_item_unreffed,
                     self);

  return res;
}

static gpointer
sample_lazy_list_get_item (GListModel *self, guint position)
{
  return get_from_cache (SAMPLE_LAZY_LIST (self), position);
}

static void
sample_lazy_list_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = sample_lazy_list_get_item_type;
  iface->get_n_items = sample_lazy_list_get_n_items;
  iface->get_item = sample_lazy_list_get_item;
}

static void
sample_lazy_list_init (SampleLazyList *self)
{
  /* Passing NULL to the data_destroy function, as in this example
   * we do not keep full references on them */
  self->artists = g_sequence_new (NULL);
}
