#include "sample-utils.h"

static void
item_created_cb (SampleList *list, SampleListItem *item)
{
  if (sample_list_item_get_index (item) % 2)
    sample_list_item_set_is_selectable (item, TRUE);
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  g_signal_connect (list, "item-created", G_CALLBACK (item_created_cb), NULL);

  /* now display to the list to the user, then ... */
  g_object_unref (list);

  return 0;
}
