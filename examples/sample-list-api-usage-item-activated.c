#include "sample-utils.h"

static void
list_item_activated_cb (SampleList *list,
                        SampleListItem *item,
                        gpointer user_data)
{
  /* Update the list item to show more information about the artist */
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  g_signal_connect (list, "item-activated",
                    G_CALLBACK (list_item_activated_cb), NULL);

  /* Now display the list to the user, then ... */

  g_object_unref (list);

  return 0;
}
