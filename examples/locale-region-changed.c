/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * To the extent possible under law, Philip Withnall has waived all copyright
 * and related or neighboring rights to this work. This work is
 * published from: United Kingdom.
 *
 * https://creativecommons.org/publicdomain/zero/1.0/
 *
 * SPDX-License-Identifier: CC0-1.0
 */

#include <glib.h>
#include <gio/gio.h>

/* Partial interface definition from
 * https://www.freedesktop.org/wiki/Software/systemd/localed/ */
static const GDBusPropertyInfo org_freedesktop_locale1_locale = {
  -1,  /* statically allocated */
  "Locale",
  "as",
  G_DBUS_PROPERTY_INFO_FLAGS_READABLE,
  NULL,  /* no annotations */
};

static const GDBusPropertyInfo *org_freedesktop_locale1_properties[] = {
  &org_freedesktop_locale1_locale,
  NULL
};

static const GDBusInterfaceInfo org_freedesktop_locale1_info = {
  -1,  /* statically allocated */
  "org.freedesktop.locale1",
  NULL,  /* no methods which we care about */
  NULL,  /* no signals */
  (GDBusPropertyInfo **) &org_freedesktop_locale1_properties,
  NULL,  /* no annotations */
};

/* Instead of displaying the current locale, this function could be used
 * as a trigger to retranslate the UI, using the new locale. */
static void
display_locale_region (GSettings *settings,
                       GDBusProxy *proxy)
{
  g_autofree gchar *region = NULL;
  g_autoptr (GVariant) system_locale = NULL;

  region = g_settings_get_string (settings, "region");
  system_locale = g_dbus_proxy_get_cached_property (proxy, "Locale");

  if (!g_str_equal (region, ""))
    {
      /* Use the user’s locale. */
      g_print ("User locale region is %s\n", region);
    }
  else if (system_locale != NULL)
    {
      g_autofree const gchar **vars = NULL;
      const gchar **i;

      /* The region is set to the default. Fall back to the system locale. */
      vars = g_variant_get_strv (system_locale, NULL);

      g_print ("System locale is:\n");
      for (i = vars; *i != NULL; i++)
        g_print (" - %s\n", *i);
    }
  else
    {
      g_printerr ("Locale is unknown.\n");
    }
}

static gboolean
locale_region_changed_cb (GSettings *settings,
                          const gchar *key,
                          gpointer user_data)
{
  GDBusProxy *proxy = G_DBUS_PROXY (user_data);

  display_locale_region (settings, proxy);

  return TRUE;
}

static void
system_locale_changed_cb (GDBusProxy *proxy,
                          GVariant *changed_properties,
                          GStrv invalidated_properties,
                          gpointer user_data)
{
  GSettings *settings = G_SETTINGS (user_data);

  display_locale_region (settings, proxy);
}

int
main (int ac,
      char **av)
{
  g_autoptr (GSettings) settings = NULL;
  g_autoptr (GDBusConnection) connection = NULL;
  g_autoptr (GDBusProxy) proxy = NULL;
  g_autoptr (GError) error = NULL;

  /* Connect to GSettings and to the system locale manager. */
  settings = g_settings_new ("org.gnome.system.locale");
  connection = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);

  if (error != NULL)
    {
      g_printerr ("Error connecting to system bus: %s\n", error->message);
      return 1;
    }

  proxy = g_dbus_proxy_new_sync (connection, G_DBUS_PROXY_FLAGS_NONE,
                                 (GDBusInterfaceInfo *) &org_freedesktop_locale1_info,
                                 "org.freedesktop.locale1",
                                 "/org/freedesktop/locale1",
                                 "org.freedesktop.locale1",
                                 NULL, &error);

  if (error != NULL)
    {
      g_printerr ("Error getting locale from bus: %s\n", error->message);
      return 1;
    }

  display_locale_region (settings, proxy);

  g_signal_connect (settings, "changed::region",
                    (GCallback) locale_region_changed_cb, proxy);
  g_signal_connect (proxy, "g-properties-changed",
                    (GCallback) system_locale_changed_cb, settings);

  while (TRUE)
    g_main_context_iteration (NULL, TRUE);

  return 0;
}
