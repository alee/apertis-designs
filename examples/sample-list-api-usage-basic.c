#include "sample-list.h"

/* In this example, our backing model is a simple GPtrArray */

typedef struct
{
  gchar *name;
} SampleArtist;

static void
free_sample_artist (SampleArtist *artist)
{
  g_free (artist->name);
  g_free (artist);
}

static SampleArtist *
create_sample_artist (const gchar *name)
{
  SampleArtist *res = g_new0 (SampleArtist, 1);

  res->name = g_strdup (name);

  return res;
}

static GPtrArray *
create_sample_artists (void)
{
  GPtrArray *res;

  res = g_ptr_array_new_with_free_func ((GDestroyNotify) free_sample_artist);

  g_ptr_array_add (res, create_sample_artist ("GYBE"));

  return res;
}

static void
create_sample_artist_item (SampleArtist *artist, GListStore *adapter)
{
  SampleListItem *item = sample_list_item_new ();
  ClutterActor *name_label =
      clutter_text_new_with_text (artist->name, "Sans 12");

  clutter_actor_add_child (CLUTTER_ACTOR (item), name_label);

  /* The list store takes ownership of the new artist item so we do not need to
   * unref it.
   */
  g_list_store_append (adapter, item);
}

SampleList *
create_sample_list (void)
{
  g_autoptr (GListStore) adapter;
  g_autoptr (SampleList) list;
  g_autoptr (GPtrArray) backend_artists = create_sample_artists ();

  adapter = g_list_store_new (SAMPLE_TYPE_LIST_ITEM);

  /* Populate the adapter with artist views */
  g_ptr_array_foreach (backend_artists, (GFunc) create_sample_artist_item,
                       adapter);

  /* Create the list widget and pass the adapter */
  list = sample_list_new ();

  /* This takes a reference to adapter */
  sample_list_set_adapter (list, G_LIST_MODEL (adapter));

  return g_steal_pointer (&list);
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  /* We may now expose the list to the user, once that is done
   * we can drop our reference to the list. This will release the
   * last remaining reference to the adapter, which will in turn
   * release the last references to individual artist items.
   */
  g_object_unref (list);

  /* All references are released */

  return 0;
}
