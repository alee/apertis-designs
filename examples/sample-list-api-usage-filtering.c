#include "sample-utils.h"

static gboolean
filter_artist (guint index)
{
  /* Here we could retrieve the artist at the specified index
   * from our backing data model, and return TRUE or FALSE
   * based on its attributes. For simplicity we only make it
   * so the first 20 items get displayed */
  return (index <= 20);
}

static void
filter_artists (GListStore *store)
{
  guint i, n_items;

  n_items = g_list_model_get_n_items (G_LIST_MODEL (store));

  for (i = 0; i < n_items; i++)
    {
      if (!filter_artist (i))
        g_list_store_remove (store, i);
    }
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  filter_artists (G_LIST_STORE (sample_list_get_adapter (list)));

  /* now display to the list to the user, then ... */
  g_object_unref (list);

  return 0;
}
