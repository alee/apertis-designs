#include "sample-list.h"

/* In this example, our backing model is a simple GList */

typedef GList BackingModel;

static void
backing_model_free (BackingModel *model)
{
  g_list_free_full (model, g_object_unref);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (BackingModel, backing_model_free)

static GList *
create_backing_model (void)
{
  GList *res = NULL;

  res = g_list_prepend (res, sample_artist_new ("GYBE"));

  return res;
}

int
main (int ac, char **av)
{
  SampleList *list = sample_list_new ();
  g_autoptr (BackingModel) backing_model = create_backing_model ();
  g_autoptr (SampleListAdapter) adapter;

  /* Create a generic adapter and pass it to the list widget.
   * In our case, the adapter will automatically create list
   * items containing a single ClutterText, representing the
   * name of the artist. The adapter essentially converts a GList of
   * SampleArtist objects to a GListModel of ClutterText objects.
   */
  adapter = sample_list_adapter_new_from_g_list (backing_model);
  sample_list_set_adapter (list, G_LIST_MODEL (adapter));

  /* We may now expose the list to the user, once that is done
   * we can drop our reference to the list. This will release the
   * last remaining reference to the adapter. */
  g_object_unref (list);

  /* All references are released */

  return 0;
}
