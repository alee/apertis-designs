#include "sample-utils.h"

static void
selected_items_changed_cb (SampleList *list)
{
  GArray *selected;

  selected = sample_list_get_selected_items (list);

  /* Now do something with the items */

  g_array_free (selected, TRUE);
}

int
main (int ac, char **av)
{
  SampleList *list = create_sample_list ();

  g_object_set (list, "selection-mode", SAMPLE_SELECTION_MULTIPLE, NULL);

  g_signal_connect (list, "selected-items-changed",
                    G_CALLBACK (selected_items_changed_cb), NULL);

  /* now display to the list to the user, then ... */
  g_object_unref (list);

  return 0;
}
