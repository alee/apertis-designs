def release = "v2021dev1"

pipeline {
  options {
    timeout(time: 20, unit: 'MINUTES')
  }
  environment {
    RELEASE = "${release}"
  }
  agent {
    docker {
      label 'docker-slave'
      image "apertis/apertis-${release}-documentation-builder"
      registryUrl 'https://docker-registry.apertis.org'
    }
  }
  stages {
    stage("Build site") {
      steps {
        sh "make"
        sh "DESTDIR=out make install"
      }
    }

    stage ("Upload site") {
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'rsync -e "ssh -oStrictHostKeyChecking=no" -va --no-group --no-owner --chmod=ugo=rwX --delete-after out/usr/share/doc/apertis-designs/apertis-designs/ docsync@designs.apertis.org:/srv/designs.apertis.org/www/${RELEASE}'
          }
        }
      }
    }

    stage("Build pdfs") {
      steps {
        sh "make pdf"
      }
    }

    stage ("Upload pdfs") {
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'rsync -e "ssh -oStrictHostKeyChecking=no" -va --no-group --no-owner --chmod=ugo=rwX --delete-after build/pdf/*.pdf docsync@designs.apertis.org:/srv/designs.apertis.org/www/${RELEASE}'
          }
        }
      }
    }

    stage ("Link latest") {
      when {
        expression {
          env.GIT_BRANCH == 'origin/apertis/v2021dev1'
        }
      }
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'ssh -oStrictHostKeyChecking=no docsync@designs.apertis.org ln -sf --no-dereference ${RELEASE} /srv/designs.apertis.org/www/latest'
          }
        }
      }
    }
  }

  post {
    always {
      deleteDir()
    }
  }
}

