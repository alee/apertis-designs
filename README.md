# Introduction

This repository contains the design documents written by Collabora
and Bosch engineers for the Apertis platform.

# Contributing

## Style

We follow the commonmark specification, extended by hotdoc's
table extensions (github-flavored pipe tables).

We prefer reference links at the bottom of individual
documents instead of inline.

We will try to follow this
[style guide](http://www.cirosantilli.com/markdown-style-guide/#about),
and are still [evaluating solutions](https://github.com/jgm/cmark/issues/131)
for *stable* automatic formatting.

80 columns line width is thus not yet enforced, but strongly suggested.

## Writing a new design document

`docs/geolocation-and-navigation.md` is an example of a well-written
design document.

Generally speaking, a new design document should:

1. Contain a brief introduction summarizing its purpose

2. Define terminology (such as acronyms) and concepts it will commonly reference.

3. Expose the use cases it will attempt to solve, and use cases that could
   have been in scope but were explicitly left out.

4. Even though it's not part of the example document, a short section
   on the security model can be helpful, even just to say there is none.

5. Formulate requirements, derived from the use cases

6. Study of existing solutions, showing how they (do not) address the
   listed use cases and requirements.

7. Present the proposed approach in detail. Having defined clear use cases
   and documented existing solutions lets us reference them heavily in this
   section.

8. Go over the initial list of requirements, and show how the proposed
   design(s) fulfil or fail them.

9. Summarize the questions that remain open, to ease future improvement

10. Summarize the recommendations that the proposed approach made.

## Submit your changes

We will use the [standard contribution process](https://wiki.apertis.org/Guidelines/Contribution_process).

# Build the documentation locally in a docker container

To avoid installing [hotdoc](https://github.com/hotdoc/hotdoc) and [pandoc](https://pandoc.org/)
by hand, the docker image used on the server side can be used to build locally as well:

```
RELEASE_VERSION="v2019"
docker run -it --rm \
    -v $(pwd):/documentation \
    -w /documentation \
    -u $(id -u) \
    --security-opt label=disable \
    docker-registry.apertis.org/apertis/apertis-${RELEASE_VERSION}-documentation-builder \
    make
```

Use run `make pdf` in the container to generate the PDFs.

# Licensing

Apertis designs are licensed under CC BY-SA 4.0 International. See COPYING
for more details.
